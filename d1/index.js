console.log("Hello, B248!");

/*
	Mini Activity
	1. Create a function named greeting() and display the message you want to say to yourself using console.log inside of the function
	2. Invoke the greeting() function 20 times
	3. Take a screenshot

*/

function greeting(){
	console.log("Hi Cee! Keep on progressing!")
};

greeting();

//we can just use our loops

let countNum = 20;

while(countNum !==0){
	console.log("This is printed inside the loop" + countNum);
	greeting();
	countNum--;
}

//While Loop

/*
	a while loop takes in an expression/condition
	- expressions are any unit of code that can be evaluated to a value
	- if the condition evaluates to true, the statement inside the code block will be executed
	- a statement is a command that the programmer gives to a computer

	Loop will iterate a certain number of times until an expression/condition is met
	
	- iteration is the term given to the repetition of statements

	Syntax

	while(expression/condition){
		statement
	}

*/

//while the count is not equal to zero
let count = 5;

while(count !== 0){
	//the current value of the count is also printed out
	console.log("While: " + count);

	//this decreases the value of count by 1 after every iteration to stop the loop when it reaches 0

	//LOOPS occupy a significant amount of memory space in our devices, kasi it runs until a certain condition is met

	// Make sure that expressions/conditions in loops have their corresponding increment/decrement operators to stop the loop

	// Forgetting to include this in loops will make our applications run an infinite loop which will eventually crash our devices

	//after running the script, if a slow response from the browser is experienced or an infinite loop is seen in the console QUICKLY CLOSE the application/browser/tab to avoid this

	count --;//DO NOT FORGET
}

//do while loop

/*

	a do while loop works a lot like while loop, but unlike while loops, do-while loops guarantee that the code will be executed at least once

	Syntax:
	do {
		statement
	} while (expression/condition)


*/

/*
    - The "Number" function works similarly to the "parseInt" function
    - The Number() method converts a value to a number. If the value cannot be converted, NaN is returned.

    - Both differ significantly in terms of the processes they undertake in converting information into a number data type and other features that help with manipulating data

    - The "prompt" function creates a pop-up message in the browser that can be used to gather user input
    - How the Do While Loop works:
        1. The statements in the "do" block executes once
        2. The message "Do While: " + number will be printed out in the console
        3. After executing once, the while statement will evaluate whether to run the next iteration of the loop based on given expression/condition (e.g. number less than 10)
        4. If the expression/condition is not true, another iteration of the loop will be executed and will be repeated until the condition is met
        5. If the expression/condition is true, the loop will stop
*/

let number = Number(prompt("Give me a number!"));

do {
	//the current value of number is printed out
	console.log("Do While: " + number);
	//increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
	//number = number + 1
	number += 1;

//Providing a number of 10 or greater will run the code block ONCE and will STOP the loop
}while(number < 10)

//For Loop

/*
    - A for loop is more flexible than while and do-while loops. It consists of three parts:
        1. The "initialization" value that will track the progression of the loop.
        2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
        3. The "finalExpression" indicates how to advance the loop.
    - Syntax
        for (initialization; expression/condition; finalExpression) {
            statement
        }
*/

for (let count = 0; count <=20; count++){
	console.log("For Loop: " + count);
}

/*
	mini activity (3 mins)

	refactor the code above that will only print the even numbers
	Take a screenshot of your work's console and send it to our batch chat


*/

for (let count = 0; count <=20; count++){
	// console.log("For Loop: " + count);
	if(count % 2 == 0){
		console.log("Even: " + count);
	}
}


let myString = "Vice Ganda"

console.log(myString.length);//10

console.log(myString[0]);//V
console.log(myString[1]);//i
console.log(myString[2]);//c
console.log(myString[9]);//a

for(let x = 0; x<myString.length;x++){
	console.log(myString[x])
}
//V
//i
//c
//e
//
//G
//a
//n
//d
//a

let myName = "Zeref Dragneel";


for (let i=0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		){

		console.log("Hi I'm a vowel!")

	}else{
		console.log(myName[i])
	}
}

//Continue and Break Statements
/*
	The continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

	The break statement is used to terminate the current loop once a match has been found

*/

for(let count = 0; count <=20; count++){

	if(count % 2 === 0){
		//Tells the code to continue to next iteration of the loop
		//This ignores all statements located after the continue statement
		continue;
	}

	console.log("Continue and Break: " + count);

	if(count>10){

		//tells the code to terminate /stop the loop even if the expression/condition of the loop defines that it should execute as longa as the value of count is less than or equal to 20
		//the other number values will no longer the printed

		break;
	}

}

let name = "Mommy Dragneel Yonisia";

for(let i = 0; i < name.length; i++){
	console.log(name[i]);

	//if the vowel is equal to a, continue to the next iteration of the loop
	if(name[i].toLowerCase()==="a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] == "Y"){
		break;
	}



}