console.log("Why did the programmer quit his job? Because he didn't get arrays.");
console.log('');


let number = parseInt(prompt("Please provide a number:"));
console.log(`The number you provided is ${number}.`);

for (let i = number; i >= 0; i--) {
    
    if (i <= 50) {
        console.log('The current value is at 50. Terminating the loop.');
        break;
    }
    if (i % 10 === 0) {
        console.log('The number is divisible by 10. Skipping the number.');
        continue;
    }
    if (i % 5 === 0) {
        console.log(i);
    }
    
};

let string = 'supercalifragilisticexpialidocious.';
let consonants = '';
for (let i = 0; i < string.length; i++) {
    let vow = string[i];
    if (vow === "a" || vow === "e" || vow === "i" || vow === "o" || vow === "u") {
      continue;
    } else {
      consonants += vow;
    }
    
}
console.log('');
console.log('Returned Letters without the Vowels:');
console.log(" >> " + consonants);
